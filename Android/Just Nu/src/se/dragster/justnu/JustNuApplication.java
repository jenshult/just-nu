package se.dragster.justnu;

import android.app.Application;
import android.util.Log;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.PushService;

public class JustNuApplication extends Application
{

   /** Called when the application is first created. */
   @Override
   public void onCreate( )
   {
      Parse.initialize( this , "i6zBwca93y8slY0jUEogQaLkoMFFDlzFnpwSqjmk" ,
         "0E2ShNTXWTT4pc4VsXxi0BVCWDmBzSozDJESULCQ" );
      setupParseUser( );
      ParseInstallation installation = ParseInstallation
         .getCurrentInstallation( );
      installation.saveEventually( );
      PushService.subscribe( getApplicationContext( ) , "" ,
         JustNuActivity.class );
      PushService.subscribe( getApplicationContext( ) , "justnu" ,
         JustNuActivity.class );
      PushService.setDefaultPushCallback( this , JustNuActivity.class );
   }


   private void setupParseUser( )
   {
      ParseACL.setDefaultACL( new ParseACL( ) , false );
      ParseAnonymousUtils.logIn( new LogInCallback( )
      {
         @Override
         public void done( ParseUser user , ParseException e )
         {
            if( e != null )
            {
               Log.d( "JustNu" , "Anonymous login failed." );
            }
            else
            {
               Log.d( "JustNu" , "Anonymous user logged in." );
            }
         }
      } );
   }
}
